# SourceGit

开源的Git客户端，仅用于Windows 10。单文件，无需安装，< 500KB。

## 预览

* DarkTheme

![Preview_Dark](./Preview_Dark.png)

* LightTheme

![Preview_Light](./Preview_Light.png)


## Thanks

* [PUMA](https://gitee.com/whgfu) 配置默认User
